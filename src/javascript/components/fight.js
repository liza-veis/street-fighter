import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const pressedKeys = new Set();
  const fightData = getFightInitialData(firstFighter, secondFighter);

  fightData.forEach(({ defenseElement }) => {
    defenseElement.style.opacity = 0;
  });

  return new Promise((resolve) => {
    const onKeyup = (event) => {
      pressedKeys.delete(event.code);
      fightData.forEach(({ defenseElement, controls: { blockKey } }) => {
        if (event.code === blockKey) defenseElement.style.opacity = 0;
      });
    };
    const onKeydown = (event) => {
      if (event.repeat) return;

      pressedKeys.add(event.code);
      fightData.forEach(({ defenseElement, controls: { blockKey } }) => {
        if (event.code === blockKey) defenseElement.style.opacity = 1;
      });

      const attackFighterIndex = getAttackFighterIndex(fightData, event.code, pressedKeys);
      const criticalHitFighterIndex = getCriticalHitFighterIndex(fightData, event.code, pressedKeys);

      if (attackFighterIndex === -1 && criticalHitFighterIndex === -1) return;

      const attackerIndex = attackFighterIndex !== -1 ? attackFighterIndex : criticalHitFighterIndex;
      const defenderIndex = +!attackerIndex;
      const defenderData = fightData[defenderIndex];

      if (attackFighterIndex !== -1 && pressedKeys.has(defenderData.controls.blockKey)) return;
      if (criticalHitFighterIndex !== -1) recharge(fightData[attackerIndex]);

      const attack = (attacker, defender) => {
        let damage = 2 * attacker.attack;

        if (criticalHitFighterIndex === -1) {
          damage = getDamage(attacker, defender);
        }

        defenderData.actualHealth = Math.max(defenderData.actualHealth - damage, 0);
        defenderData.healthIndicator.style.width = `${(defenderData.actualHealth * 100) / defender.health}%`;
      };

      if (defenderIndex) {
        attack(firstFighter, secondFighter);
      } else {
        attack(secondFighter, firstFighter);
      }

      if (defenderData.actualHealth === 0) {
        document.removeEventListener('keydown', onKeydown);
        document.removeEventListener('keyup', onKeyup);
        resolve([firstFighter, secondFighter][attackerIndex]);
      }
    };

    document.addEventListener('keydown', onKeydown);
    document.addEventListener('keyup', onKeyup);
  });
}

function getFightInitialData(firstFighter, secondFighter) {
  return [
    {
      actualHealth: firstFighter.health,
      isRecharged: true,
      healthIndicator: document.getElementById('left-fighter-indicator'),
      rechargeIndicator: document.getElementById('left-fighter-recharge-indicator'),
      defenseElement: document.getElementById('left-fighter-defense'),
      controls: {
        attackKey: controls.PlayerOneAttack,
        blockKey: controls.PlayerOneBlock,
        criticalHitCombination: controls.PlayerOneCriticalHitCombination,
      },
    },
    {
      actualHealth: secondFighter.health,
      isRecharged: true,
      healthIndicator: document.getElementById('right-fighter-indicator'),
      rechargeIndicator: document.getElementById('right-fighter-recharge-indicator'),
      defenseElement: document.getElementById('right-fighter-defense'),
      controls: {
        attackKey: controls.PlayerTwoAttack,
        blockKey: controls.PlayerTwoBlock,
        criticalHitCombination: controls.PlayerTwoCriticalHitCombination,
      },
    },
  ];
}

function recharge(...fightersData) {
  fightersData.forEach((fighterData) => {
    let charge = 0;

    fighterData.isRecharged = false;
    fighterData.rechargeIndicator.style.width = 0;

    const interval = setInterval(() => {
      charge++;
      fighterData.rechargeIndicator.style.width = `${charge * 10}%`;
      if (charge === 10) {
        fighterData.isRecharged = true;
        clearInterval(interval);
      }
    }, 1000);
  });
}

function getAttackFighterIndex(fightData, key, pressedKeys) {
  return fightData.findIndex(({ controls }) => {
    const { attackKey, blockKey } = controls;

    return attackKey === key && !pressedKeys.has(blockKey);
  });
}

function getCriticalHitFighterIndex(fightData, key, pressedKeys) {
  return fightData.findIndex(({ isRecharged, controls }) => {
    const { criticalHitCombination } = controls;
    const isCriticalHitCombination = criticalHitCombination.every((key) => pressedKeys.has(key));

    return isRecharged && criticalHitCombination.includes(key) && isCriticalHitCombination;
  });
}

export function getDamage(attacker, defender) {
  const blockPower = defender ? getBlockPower(defender) : 0;

  return Math.max(getHitPower(attacker) - blockPower, 0);
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;

  return defense * dodgeChance;
}
