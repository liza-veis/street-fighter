import { fighterService } from '../../services/fightersService';
import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { createFighters } from '../fightersView';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const rootElement = document.getElementById('root');
  const loadingElement = document.getElementById('loading-overlay');
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterImage = createFighterImage(fighter);
  const title = `${fighter.name} won!`;

  bodyElement.append(fighterImage);

  const onClose = async () => {
    root.innerHTML = '';

    try {
      loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    } finally {
      loadingElement.style.visibility = 'hidden';
    }
  };

  showModal({ title, bodyElement, onClose });
}
