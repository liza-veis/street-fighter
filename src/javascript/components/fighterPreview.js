import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) return fighterElement;

  const imageElement = createFighterImage(fighter);
  const fighterProperties = ['name', 'health', 'attack', 'defense'];
  const fighterPropertiesElement = createElement({ tagName: 'span', className: 'fighter-preview___properties' });

  fighterProperties.forEach((property) => {
    const propertyElement = createElement({ tagName: 'span', className: `fighter-preview___${property}` });
    propertyElement.innerText = fighter[property];
    fighterPropertiesElement.append(propertyElement);
  });

  fighterElement.append(imageElement, fighterPropertiesElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
